package sound;

import util.Array;

import javax.sound.sampled.*;
import java.io.*;

public class Sound {

    private AudioFormat af = null;
    private byte[] data = null;

    public Sound() {
    }

    private Sound(AudioFormat af, byte[] data) {
        this.af = af;
        this.data = data;
    }

    public Sound(String pathFile) throws IOException, UnsupportedAudioFileException {
        File soundFile = new File(pathFile);
        WaveFile(soundFile);
    }

    private Sound clone(byte[] data) {
        return new Sound(this.af, data);
    }

    public AudioFormat getAf() {
        return af;
    }

    /**
     * Создает объект из указанного wave-файла
     *
     * @param file - wave-файл
     * @throws UnsupportedAudioFileException a
     * @throws IOException a
     */
    private void WaveFile(File file) throws UnsupportedAudioFileException, IOException {

        if(!file.exists()) {
            throw new FileNotFoundException(file.getAbsolutePath());
        }

        // получаем поток с аудио-данными
        AudioInputStream ais = AudioSystem.getAudioInputStream(file);

        // получаем информацию о формате
        af = ais.getFormat();

        // количество кадров в файле
        long framesCount = ais.getFrameLength();

        // размер данных в байтах
        long dataLength = framesCount*af.getSampleSizeInBits()*af.getChannels()/8;

        // читаем в память все данные из файла разом
        data = new byte[(int) dataLength];
        ais.read(data);
    }

    public Sound plus(Sound sound) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(data, 0, data.length);
        byteArrayOutputStream.write(sound.getDate(), 0, sound.getDate().length);
        return this.clone(Array.concatArray(new byte[0], byteArrayOutputStream.toByteArray()));
    }

    /**
     * Сохраняет объект WaveFile в стандартный файл формата WAVE
     *
     * @throws IOException a
     */
    public void save(String pathFile) throws IOException{
        AudioSystem.write( new AudioInputStream(new ByteArrayInputStream(data), af, getFramesCount()), AudioFileFormat.Type.WAVE, new File(pathFile));
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getDate() {
        return data;
    }

    public Sound cut(int start, int finish) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(getDate(), start, finish - start);
        return this.clone(Array.concatArray(new byte[0], byteArrayOutputStream.toByteArray()));
    }

    private long getFramesCount() {
        return data.length / af.getSampleSizeInBits() / af.getChannels() * 8;
    }
}
