package sound;

import javax.sound.sampled.AudioFormat;

public class Conversion {

    private AudioFormat af;

    public Conversion(AudioFormat af) {
        this.af = af;
    }

    public int secToByte(double seconds) {
        return (int) ((af.getFrameRate() / 1000 * af.getSampleSizeInBits() * af.getChannels() / 8) * 1024 * seconds);
    }
}
