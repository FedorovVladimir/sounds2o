import org.junit.jupiter.api.Test;
import sound.Conversion;
import sound.Sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSound {

    @Test
    void TwoToOneTest() {
        Sound soundFirst = new Sound();
        soundFirst.setData(new byte[]{1,2});

        Sound soundSecond = new Sound();
        soundSecond.setData(new byte[]{5,6});

        Sound targetSound = soundFirst.plus(soundSecond);
        assertArrayEquals(new byte[]{1,2,5,6}, targetSound.getDate());
    }

    @Test
    void CutPathSound() {
        Sound sound = new Sound();
        sound.setData(new byte[]{1,2,3,4,5,6,7});
        Sound targetSound = sound.cut(2,5);
        assertArrayEquals(new byte[]{3,4,5}, targetSound.getDate());
    }

    @Test
    void SecToByte() throws IOException, UnsupportedAudioFileException {
        Sound sound = new Sound("src\\test\\resources\\Sculptor.wav");
        AudioFormat af = sound.getAf();
        Conversion conversion = new Conversion(af);
        int start = conversion.secToByte(0);
        assertEquals(0, start);
    }

    @Test
    void SecToByte2() throws IOException, UnsupportedAudioFileException {
        Sound sound = new Sound("src\\test\\resources\\Sculptor.wav");
        AudioFormat af = sound.getAf();
        Conversion conversion = new Conversion(af);
        int start = conversion.secToByte(86);
        assertEquals(sound.getDate().length, start, 1000000);
    }

    @Test
    void TwoToOneCutTest() {
        Sound soundFirst = new Sound();
        soundFirst.setData(new byte[]{1,2,3,4,5,6});

        Sound soundSecond = new Sound();
        soundSecond.setData(new byte[]{5,6,7,8,9,10});

        Sound targetSound = soundFirst.cut(0,5).plus(soundSecond.cut(1,6));

        assertArrayEquals(new byte[]{1,2,3,4,5,6,7,8,9,10}, targetSound.getDate());
    }
}